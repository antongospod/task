import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import jQuery from 'jquery'
window.$ = window.jQuery = jQuery
import 'popper.js'
import 'bootstrap'
import './bootstrap/bootstrap.scss'

Vue.config.productionTip = false

router.beforeEach((to, from, next)=>{
  const isLogin = localStorage.getItem('token') == 'Login' ;
  if( isLogin ){
    next();
  } else {
    if( to.path !== '/login')
      next('/login');
    else
      next();
  }
});

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
