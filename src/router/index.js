import Vue from 'vue'
import VueRouter from 'vue-router'
import Login from '../views/Login.vue'
import Header from '../views/Header.vue';
import Home from '../views/Home.vue';
import About from '../views/About.vue';

Vue.use(VueRouter)

  const routes = [
  {
    path: '/login',
    name: 'Login',
    component: Login
  },
  {
    path: '/',
    components: {
      default: Home,
      nav: Header
    }
  },
  {
    path: '/about',
    name: 'About',
    components: {
      default: About,
      nav: Header
    }
  },
  {
    path: '*',
    redirect: '/',
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
